#! /usr/bin/python
# coding: utf-8

class GitLab(object):
    """GitLab Class

    """
    def __init__(self, name, width, height):
        self._name = name
        self._width = width
        self._height = height
    
    @property
    def size(self):
        return self._height * self._width

if __name__ == '__main__':
    lab = GitLab("gitlab", 10, 20)
    if True:
        for i in xrange(2):
        print "(%03d) Size: %d" % (i, lab.size)
